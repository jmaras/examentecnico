package examen.tecnico.intivefdv.bicis;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import examen.tecnico.intivefdv.alquiler.bicis.AlquilerFamiliar;
import examen.tecnico.intivefdv.alquiler.bicis.AlquilerPorDia;
import examen.tecnico.intivefdv.alquiler.bicis.AlquilerPorHora;
import examen.tecnico.intivefdv.alquiler.bicis.AlquilerPorSemana;
import examen.tecnico.intivefdv.alquiler.bicis.exception.AlquilerFamiliarMasDeCincoException;
import examen.tecnico.intivefdv.alquiler.bicis.exception.AlquilerFamiliarMenosDeTresExecption;

public class AlquilerFamiliarTest {

	@Test
	public void alquilerFamiliarCalcularTotalCorrecto595() throws Exception {
		AlquilerPorDia alquilerPorDia = new AlquilerPorDia();
		AlquilerPorHora alquilerPorHora = new AlquilerPorHora();
		AlquilerPorSemana alquilerPorSemana = new AlquilerPorSemana();
		AlquilerFamiliar alquilerFamiliar = new AlquilerFamiliar();
		alquilerFamiliar.agregarAlquiler(alquilerPorHora);
		alquilerFamiliar.agregarAlquiler(alquilerPorSemana);
		alquilerFamiliar.agregarAlquiler(alquilerPorDia);
		assertEquals(new Float(59.5), alquilerFamiliar.calcularTotal());

	}

	@Test(expected = AlquilerFamiliarMasDeCincoException.class)
	public void alquilerFamiliarMasDeCincoException() throws Exception {
		AlquilerPorHora alquilerPorHora = new AlquilerPorHora();
		AlquilerPorSemana alquilerPorSemana = new AlquilerPorSemana();
		AlquilerFamiliar alquilerFamiliar = new AlquilerFamiliar();
		alquilerFamiliar.agregarAlquiler(alquilerPorHora);
		alquilerFamiliar.agregarAlquiler(alquilerPorSemana);
		alquilerFamiliar.agregarAlquiler(alquilerPorSemana);
		alquilerFamiliar.agregarAlquiler(alquilerPorSemana);
		alquilerFamiliar.agregarAlquiler(alquilerPorSemana);
		alquilerFamiliar.agregarAlquiler(alquilerPorSemana);
	}
	
	@Test(expected = AlquilerFamiliarMenosDeTresExecption.class)
	public void alquilerFamiliarMenosDeTresExecption() throws Exception {
		AlquilerPorHora alquilerPorHora = new AlquilerPorHora();
		AlquilerPorSemana alquilerPorSemana = new AlquilerPorSemana();
		AlquilerFamiliar alquilerFamiliar = new AlquilerFamiliar();
		alquilerFamiliar.agregarAlquiler(alquilerPorHora);
		alquilerFamiliar.agregarAlquiler(alquilerPorSemana);
		alquilerFamiliar.calcularTotal();
	}
}
