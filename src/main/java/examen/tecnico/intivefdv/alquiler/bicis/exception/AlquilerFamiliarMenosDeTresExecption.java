package examen.tecnico.intivefdv.alquiler.bicis.exception;

public class AlquilerFamiliarMenosDeTresExecption extends Exception {
	public AlquilerFamiliarMenosDeTresExecption(String message) {
		super(message);
	}
}
