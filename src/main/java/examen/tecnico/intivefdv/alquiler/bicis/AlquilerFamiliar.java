package examen.tecnico.intivefdv.alquiler.bicis;

import java.util.ArrayList;
import java.util.List;

import examen.tecnico.intivefdv.alquiler.bicis.exception.AlquilerFamiliarMasDeCincoException;
import examen.tecnico.intivefdv.alquiler.bicis.exception.AlquilerFamiliarMenosDeTresExecption;

public class AlquilerFamiliar {
	
	private List<Alquiler> listaAlquileres = new ArrayList<Alquiler>();
	
	public AlquilerFamiliar() {}

	public List<Alquiler> getListaAlquileres() {
		return listaAlquileres;
	}

	public void setListaAlquileres(List<Alquiler> listaAlquileres) throws AlquilerFamiliarMasDeCincoException {
		this.listaAlquileres = listaAlquileres;
	}
	
	public void agregarAlquiler(Alquiler alquiler) throws AlquilerFamiliarMasDeCincoException {
		if(listaAlquileres.size() < 5) {
			listaAlquileres.add(alquiler);
		} else {
			throw new AlquilerFamiliarMasDeCincoException("No se pueden agregar mas de 5 alquileres");
		}
	}
	
	public Float calcularTotal() throws AlquilerFamiliarMenosDeTresExecption  {
		if(listaAlquileres.size() >= 3) {
			Float total = new Float(0);
			for(Alquiler aux : listaAlquileres) {
				total = total + aux.obtenerTotalAlquiler();
			}
			return new Float( total - (total.floatValue() * 0.30));
		} else {
			throw new AlquilerFamiliarMenosDeTresExecption("Se requieren al menos 3 alquileres");
		}
	}
	
}
