package examen.tecnico.intivefdv.alquiler.bicis.exception;

public class AlquilerFamiliarMasDeCincoException extends Exception {
	public AlquilerFamiliarMasDeCincoException(String message) {
		super(message);
	}
}
