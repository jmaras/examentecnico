package examen.tecnico.intivefdv.alquiler.bicis;

public abstract class Alquiler {
	
	private Float precio;
	private Integer cantidad = 1;
	
	public Alquiler(Float precio) {
		this.precio = precio;
	}

	public Float obtenerTotalAlquiler() {
		return precio * cantidad;
	}
	
	public void sumarCantidad() {
		cantidad++;
	}
	
	
}
