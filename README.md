### Examen tecnico intivefdv ###

### Requisitos ###

Para poder probar los tests unitarios es necesario tener instalado maven, jdk18_144 y configurado la variable de entorno JAVA_HOME previamente con la ruta del jdk

### Ejecutar test ###
Por linea de comando ejecutar las siguientes lineas

cd path_to_repo\
path_to_maven_install\bin\mvn test


### Explicacion de resolucion ###

Para resolver el problema se desarrollaron 5 clases, Alquiler (abstracta), AlquilerPorHora, AlquilerPorDia, AlquilerPorSemana y AlquilerFamiliar.
En cuanto a las 4 primeras clases AlquilerPorHora, AlquilerPorDia y AlquilerPorSemana heredan de Alquiler para poder realizar una lista en AlquilerFamiliar que contemple los 3 casos de tipo de Alquiler.
Se desarrollaron 2 excepciones de usuarios para que no se pueda realizar el calculo del total con menos de 3 alquileres y con mas de 5 alquileres en AlquilerFamiliar.